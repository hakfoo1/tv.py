#!/usr/bin/python3
import sys;
import os;
import json;
import requests;
from PIL import Image, ImageDraw, ImageFont
from tkinter import ttk;
import tkinter as tk;
import mpv;
import atexit;

import threading;




def keyParser(event) :
	global channelBuffer;
	global selectedChannel;
	global visibleChannels;
	global visibleChannelIds;
	if event.keysym == "Escape" :
		channelBuffer = "";
		selectedChannel = 0;	
		clearOSD();
		return;
	elif event.keysym == "BackSpace" :
		channelBuffer = channelBuffer[:len(channelBuffer) - 1]
		selectedChannel = 0;
	elif event.keysym == "Return" or event.keysym == 'KP_Enter':
		changeChannel();
		return;
	elif event.keysym == "Up" :
		selectedChannel -= 1;
	elif event.keysym == "Down" :
		selectedChannel += 1;
	else :
		if (len(event.char) != 1 or event.char < ' ') :
			return;
		channelBuffer += event.char;
		selectedChannel = 0;

	# prepare filtered list from the channelList
	visibleChannels = {};
	visibleChannelIds = {};
	for counter, channel in enumerate(channelList["channels"]):
		if (channel["GuideName"].lower().startswith(channelBuffer.lower()) or
		channel["GuideNumber"].lower().startswith(channelBuffer.lower())) :
			visibleChannels[len(visibleChannels)] = channel;
			visibleChannelIds[len(visibleChannelIds)] = counter;
	
	# move "cursor" within range of possible selections		
	if selectedChannel >= len(visibleChannels) :
		selectedChannel = len(visibleChannels) - 1;
	if selectedChannel < 0 :
		selectedChannel = 0;
		
	drawOSD();
	
def drawOSD():	
	global overlay;
	global visibleChannels;

	img = Image.new("RGBA", (600, 1400), (255, 255, 255,0));
	d = ImageDraw.Draw(img);
	d.text((2, 2), channelBuffer,fill=(0,0,0,255), font=bigfont);
	d.text((0, 0), channelBuffer,fill=(255,255,0,255), font=bigfont);
	offset = 38;
	
	# if the cursor is N channels down, we want to skip the N-5 so that the display "scrolls"; if it's less than 5 down, we can leave all the top entries.

	skip = max(selectedChannel - 5, 0);
	
	for id in visibleChannels :		
		if id < skip :
			continue;
		# the selected channel is blue, and the others are greyed out.
		offset += 36;
		d.text((2, offset + 2), str.rjust(visibleChannels[id]["GuideNumber"], 6) + " - " + visibleChannels[id]["GuideName"], fill=(0,0,0,128), font=font);
		if id == selectedChannel:
			fill = (127,127, 255, 255);
		else :
			fill = (220, 220, 220, 255);
		d.text((0, offset), str.rjust(visibleChannels[id]["GuideNumber"], 6) + " - " + visibleChannels[id]["GuideName"], fill=fill, font=font);
			
	overlay.update(img, pos=(20, 20));
	queueClearOSD();
	
def queueClearOSD() :
	global timer;
	if (timer) :
		timer.cancel();
	timer = threading.Timer(5, clearOSD);
	timer.start();
	

def quitter():
	window.destroy();
	exit(0);
	
def updateConfigFile():
	f = open(os.path.expanduser("~")+"/.tvrc", "w");
	f.write(json.dumps(channelList));
	f.close();
		
def clearOSD():
	global channelBuffer;
	global selectedChannel;
	global overlay;
	channelBuffer = "";
	selectedChannel = 0;
	img = Image.new("RGBA", (600, 1400), (255, 255, 255,0));
	d = ImageDraw.Draw(img);
	overlay.update(img);
	
def changeChannel() :
	global visibleChannels;
	global visibleChannelIds;
	global selectedChannel;
	
	# map back from the visible list to the full list.
	# this will be saved back to .tvrc -- we might want the 11th channel and figure out it's 45.5 later.
	channelList["selected"] = visibleChannelIds[int(selectedChannel)];
	
	
	# visibleChannels contains the entire data structure for each channel so we can still pick the details from there.
	media.play(visibleChannels[int(selectedChannel)]["URL"]);
	window.title(visibleChannels[int(selectedChannel)]["GuideNumber"] + " - " + visibleChannels[int(selectedChannel)]["GuideName"]);

	
	# also resets the partial entry data
	clearOSD();
	
def fullScreenToggle() :
	global fullscreen;
	global oldGeometry;
	if (fullscreen) :
		window.attributes("-fullscreen", False);
		fullscreen = False;
		window.geometry(oldGeometry);
	else :
		# Saving geometry doesn't work until the first time the window is moved, it assumes +0+0
		oldGeometry = (str)(window.winfo_width()) + "x" + (str)(window.winfo_height()) + "+" + (str)(window.winfo_rootx()) + "+" + (str)(window.winfo_rooty());
		window.attributes("-fullscreen", True);
		fullscreen = True;
		
def onTopToggle() :
	global onTop;
	if (onTop) :
		window.lift();
		img = Image.new("RGBA", (760, 1400), (255, 255, 255,0));
		d = ImageDraw.Draw(img);
		d.text((760,2), "Stacking",fill=(0,0,0,255), font=bigfont, anchor="rt");
		d.text((758,0), "Stacking",fill=(255,255,0,255), font=bigfont, anchor="rt");
		overlay.update(img, pos=(window.winfo_width() - 780, 20));
		queueClearOSD();
		onTop = False;
	else :
		window.attributes("-topmost", True);
		img = Image.new("RGBA", (760, 1400), (255, 255, 255,0));
		d = ImageDraw.Draw(img);
		d.text((760,2), "On Top",fill=(0,0,0,255), font=bigfont, anchor="rt");
		d.text((758,0), "On Top",fill=(255,255,0,255), font=bigfont, anchor="rt");
		overlay.update(img, pos=(window.winfo_width() - 780, 20));
		queueClearOSD();
		onTop = True;
	# This either sets up another lift, in 500ms, or removes the timer to do so.
	queueLifter();
	
def queueLifter() :
	global liftTimer;
	if (liftTimer) :
		timer.cancel();
	if (onTop) :
		liftTimer = threading.Timer(0.016, liftRetry);
		liftTimer.start();

def liftRetry() :
	if (onTop) :
		window.lift();
		liftTimer = threading.Timer(0.016, liftRetry);
		liftTimer.start();
	
	
	
def volDown():
	global volume;
	volume -= 10;
	if volume < 0 :
		volume = 0;
	showVolume();

def volUp():
	global volume;
	volume += 10;
	if volume > 100 :
		volume = 100;		
	showVolume();
	
def showVolume():
	global volume;
	global timer;
	media.ao_volume = volume;
	img = Image.new("RGBA", (760, 1400), (255, 255, 255,0));
	d = ImageDraw.Draw(img);
	d.text((760,2), "Volume " + str(volume) + "%",fill=(0,0,0,255), font=bigfont, anchor="rt");
	d.text((758,0), "Volume " + str(volume) + "%",fill=(255,255,0,255), font=bigfont, anchor="rt");
	overlay.update(img, pos=(window.winfo_width() - 780, 20));
	queueClearOSD();
	
def saveConfigIfNewChannel() :
	global channnelList, originalSelected;
	if (originalSelected != channelList["selected"]) :
		updateConfigFile();


fullscreen = False;
onTop = False;
oldGeometry = "1280x720+200+200";
font = ImageFont.truetype("ModernDOS8x8.ttf", 64);
bigfont = ImageFont.truetype("ModernDOS8x8.ttf", 128);
timer = None;
liftTimer = None;

# details about the UI channel list subset
channelBuffer = "";
selectedChannel = 0;
visibleChannels = [];
visibleChannelIds = [];
volume = 100;	

epgData = {};

# If we call it as "tv.py 192.168.123.45", we'll build an initial .tvrc from the device's lineup file.

if len(sys.argv) > 1:
	file = requests.get('http://' + sys.argv[1] + "/lineup.json");
	channelList= dict(selected =  0);
	channelList["channels"] = json.loads(file.text);
	updateConfigFile();
else:
	# otherwise, read the channel list and selection that way.
	f = open(os.path.expanduser("~") + "/.tvrc", "r");
	channelList = json.load(f);
	f.close();

selectedChannel = int(channelList["selected"]);
originalSelected = selectedChannel;

atexit.register(saveConfigIfNewChannel);
window = tk.Tk();
window.title("TV Control");
window.protocol("WM_DELETE_WINDOW", quitter);
window.geometry("1280x720");
# Key gives a "char" value, but KeyRelease does not.
window.bind("<Key>", keyParser);


media = mpv.MPV(wid=str(int(window.winfo_id())), volume="100");
overlay = media.create_image_overlay();

@media.on_key_press('MOUSE_BTN0_DBL')
def my_mouse_btn0_dbl_binding():
	fullScreenToggle();
	
@media.on_key_press('MOUSE_BTN2')
def my_mouse_btn0_dbl_binding():
	onTopToggle();
	
@media.on_key_press('WHEEL_DOWN')
def my_wheel_down_binding():
	volDown();

@media.on_key_press('WHEEL_UP')
def my_wheel_up_binding():
	volUp();
	
# This will fire once the player is active and starts playing and forces the volume to a sane default
# so if mpv saved it from the last run, we don't get stuck with unexpected silence.
# we can't hit media.ao_volume until the player is sort of active.
# It abandons itself after it sets the volume once.
@media.property_observer("core-idle")
def active_handler(name, val):
	if (val == False) :
		media.ao_volume=100;
		active_handler.unobserve_mpv_properties();



media.play(channelList["channels"][int(channelList["selected"])]["URL"]);
window.title(channelList["channels"][int(channelList["selected"])]["GuideNumber"] + " - " + channelList["channels"][int(channelList["selected"])]["GuideName"]);
window.mainloop();
