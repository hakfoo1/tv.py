# tv.py

A convenient "TV Tuner" program designed for HDHomerun devices.  While a full-fat media player can *tune* the streams provided by a HDHomerun, they tend to not have great affordances for the idea of "changing channels" where you usually just want to type "30.8" rather than scrolling through a playlist.

## Dependencies

* Python 3
* Common python libraries (signal, json,os, sys, PIL, Tkinter)
* Requests library (`pip install requests`)
* Python-mpv (https://github.com/jaseg/python-mpv `pip install mpv`)
* mpv
* Modern DOS TrueType fonts are the default for the OSD (https://notabug.org/HarvettFox96/ttf-moderndos) and should be installed.

## Setup

Depending on your environment, you may be coaxed into installing Python extensions into a venv, in which case, change the `#!/usr/bin/python3` on line 1 to the executable in that env, i. e. `#!/home/username/tvpy/bin/python3`

If you want different OSD fonts, change the references to `ModernDOS8x8.ttf`

The first time you run tv.py, you should run it with an IP address as a command-line argument.  This is the IP address of your HDHomerun device.  It will query it for the current channel list file and generate a `.tvrc` file based on it.  Future runs should just be `tv.py` unless you want to regenerate the channel list (i. e. after a rescan)

## Operations

The window defaults to 1280x720.  You can resize it, or double-click to toggle full-screen.  The scroll wheel controls volume.  Typing into the window will start filtering available channels, either by number or name.  You'll see the enterred data in big red text on the on-screen display, along with a list of matching channels below it.  The highlighted one, in blue, will be activated if you press the carraige return.  You can also move the highlight with the up and down keys, so you can start enterring part of a channel to filter, then arrow to the one you want.  A right click will toggle between "stacking" (normal) mode and "On Top" which bumps the window 60 times per second to fight anything going above it.

The volume and geometry are discarded on close, but we save the channel each time you change it.

## Authors and acknowledgment
This wouldn't be here in anything like its current form without jaseg's python-mpv library.  It was originally designed as a seperate "remote" app that spawned VLC and sent it control directions over the VLC API, but that's exactly as janky as it sounds.

## License
See LICENSE file.
